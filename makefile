CXXFLAGS = -g -std=c++11
LFLAGS   = -g -lncurses
SRCDIR   = src
OBJDIR   = obj
BINDIR   = bin
TARGET   = clte

# grab all source cpp files
SOURCES := $(wildcard $(SRCDIR)/*.cpp)
# create list of objects required for build
OBJECTS := $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)

# link all .o files & create exec (bin directory prerequisite)
$(BINDIR)/$(TARGET): $(OBJECTS) | $(BINDIR)
	$(CXX) $(OBJECTS) -o $@ $(LFLAGS)

# compile each .cpp file (obj directory prerequisite)
$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp | $(OBJDIR)
	$(CXX) $(CXXFLAGS) -c $< -o $@

# create obj & bin directories when needed
$(OBJDIR) $(BINDIR):
	@mkdir -p $@
	@echo "Created '$@' directory"

# deletes bin & obj directories
clean:
	@rm -rf $(OBJDIR)
	@rm -rf $(BINDIR)
	@echo "Deleted '$(OBJDIR)' & '$(BINDIR)' directories."