#include "chstring.h"

#include <iostream>

chstring::chstring() 
  : m_Buffer(createBuffer(BASE_CHSTRING_SIZE)), m_Size(BASE_CHSTRING_SIZE), m_Count(0) {
  
}

chstring::chstring(const std::string& str) 
  : m_Buffer(createBuffer(str.length() + 1)), m_Size(str.length() + 1), m_Count(str.length()) {

  const char* strBuffer = str.data();
  for (unsigned int i = 0; i < m_Count; i++) {
    m_Buffer[i] = strBuffer[i] | A_UNDERLINE | A_ITALIC;
  }
}

chstring::chstring(const chstring& src, unsigned int start, unsigned int end) 
  : m_Size(end - start + 1), m_Count(end - start) {

  if (end == -1) {
    end = src.length();
    m_Count = end - start;
    m_Size = m_Count + 1;
  }

  m_Buffer = createBuffer(m_Size);

  const chtype* srcBuffer = src.data();
  for (unsigned int i = 0; i < m_Count; i++) {
    m_Buffer[i] = srcBuffer[start + i];
  }
}

chstring::~chstring() {
  if (m_Buffer) {
    delete[] m_Buffer;
  }
}

void chstring::add(const char c, unsigned int index/* = -1*/) {
  add((chtype) c, index);
}

void chstring::add(const int c, unsigned int index/* = -1*/) {
  add((chtype) c, index);
}

void chstring::add(const chtype c, unsigned int index/* = -1*/) {
  ensureCapacity(1);
  
  if (index == -1 || index >= m_Count) {
    m_Buffer[m_Count++] = c;
  } else {
    for (unsigned int i = m_Count; i > index; i--) {
      m_Buffer[i] = m_Buffer[i - 1];
    }
    m_Buffer[index] = c;
    m_Count++;
  }
}

void chstring::append(const chstring& src) {
  ensureCapacity(src.length());

  const chtype* srcBuffer = src.data();
  for (unsigned int i = 0; i < src.length(); i++) {
    m_Buffer[m_Count++] = srcBuffer[i];
  }
}

void chstring::erase(unsigned int index) {
  for (unsigned int i = index; i < m_Count; i++) {
    m_Buffer[i] = m_Buffer[i + 1];
  }
  m_Count--;
}

std::string chstring::string() {
  char* buffer = new char[m_Count + 1];
  for (unsigned int i = 0; i < m_Count; i++) {
    buffer[i] = (char) (m_Buffer[i] & A_CHARTEXT);
  }
  buffer[m_Count] = 0;
  std::string str(buffer);
  
  delete buffer;
  return str;
}

chtype* chstring::createBuffer(unsigned int size) {
  chtype* newBuffer = new chtype[size];
  //Set all to null (for safety)
  for (unsigned int i = 0; i < size; i++) {
    newBuffer[i] = 0;
  }
  return newBuffer;
}

void chstring::ensureCapacity(unsigned int size) {
  if (m_Count + size >= m_Size) {
    chtype* newBuffer = createBuffer(m_Size + size + 1);
    for (unsigned int i = 0; i < m_Count; i++) {
      newBuffer[i] = m_Buffer[i];
    }

    delete[] m_Buffer;
    m_Buffer = newBuffer;
    m_Size = m_Size + size + 1;
  }
}

std::ostream& operator<<(std::ostream& stream, const chstring& chstring) {
  const chtype* data = chstring.data();
  while(*data) {
    stream << (char) (*data & A_CHARTEXT); //removes ncurses attributes
    data++;
  }
  return stream;
}