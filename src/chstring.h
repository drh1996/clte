#pragma once

#include <curses.h>
#include <string>

#define MAX_CHSTRING_SIZE 100
#define BASE_CHSTRING_SIZE 10
#define INC_CHSTRING_SIZE 2

class chstring {
  public:
    chstring();
    chstring(const std::string& str);
    chstring(const chstring& src, unsigned int start, unsigned int end = -1);
    ~chstring(); 

    void add(const char c, unsigned int index = -1);
    void add(const int c, unsigned int index = -1);
    void add(const chtype c, unsigned int index = -1);

    void append(const chstring& src);

    void erase(unsigned int index);

    std::string string();

    inline const chtype* data() const { return m_Buffer; }
    inline unsigned int length() const { return m_Count; }

    friend std::ostream& operator<< (std::ostream& stream, const chstring& chstring);

  private:
    chtype* createBuffer(unsigned int size);
    void ensureCapacity(unsigned int size);

  private:
    chtype* m_Buffer;
    unsigned int m_Size;
    unsigned int m_Count;
};