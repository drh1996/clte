#include "commandWindow.h"

CommandWindow::~CommandWindow() {
  std::cerr << "Command string: '" << *m_CmdLine << '\'' << std::endl;
  delete m_CmdLine;
}

void CommandWindow::handleInput(int c) {
  std::cerr << "CommandWindow: Key Press '" << keyname(c) << "' (" << c << ')' << std::endl;
  switch (c) {
    case KEY_UP:
    case KEY_DOWN: 
    case KEY_LEFT:
    case KEY_RIGHT:
      handleArrowKeys(c);
      break;
    case KEY_BACKSPACE:
    case 127:
    case '\b':
      backspace();
      break;
    case KEY_ENTER: 
    case '\n':
      executeCommand();
      break;
    default:
      m_CmdLine->add((char) c, m_CursorX++);
  }
}

void CommandWindow::draw() {
  wclear(m_Window);
  mvwaddchstr(m_Window, 0, 0, m_CmdLine->data());
  wmove(m_Window, 0, m_CursorX);
}

void CommandWindow::handleArrowKeys(int c) {
  if (c == KEY_UP) {
    m_CursorX = 0;
  } else if (c == KEY_DOWN) {
    m_CursorX = m_CmdLine->length();
  } else if (c == KEY_LEFT) {
    if (m_CursorX > 0) {
      m_CursorX--;
    }
  } else if (c == KEY_RIGHT) {
    int endOfLine = m_CmdLine->length();
    if (m_CursorX < endOfLine) {
      m_CursorX++;
    }
  }
}

void CommandWindow::backspace() {
  if (m_CursorX > 0) {
    m_CmdLine->erase(--m_CursorX);
  }
}

void CommandWindow::executeCommand() {
  std::cerr << "Executing command: '" << *m_CmdLine << '\'' << std::endl;
  std::string str = m_CmdLine->string();
  if (str == "q") {
    m_ShouldExitProgram = true;
  }
}
