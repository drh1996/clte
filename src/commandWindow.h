#pragma once

#include "subwindow.h"
#include "chstring.h"

class CommandWindow : public SubWindow {
  public: 
    CommandWindow(WINDOW* parent, int x, int y, int width, int height)
      : SubWindow(parent, x, y, width, height), m_CmdLine(new chstring()), m_CursorX(0), m_ShouldExitProgram(false) { 
    }
    ~CommandWindow();

    void handleInput(int c);
    void draw();
  
    inline bool shouldExitProgram() const { return m_ShouldExitProgram; }
  private:
    void executeCommand();
    void handleArrowKeys(int c);
    void backspace();

  private:
    chstring* m_CmdLine;
    unsigned int m_CursorX;
    bool m_ShouldExitProgram;
};