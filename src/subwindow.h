#pragma once

#include <ncurses.h>
#include <iostream>

class SubWindow {
  public:
    SubWindow(WINDOW* parent, int x, int y, int width, int height) 
      : m_Parent(parent), m_Window(derwin(parent, height, width, y, x)) {
    }
    
    virtual ~SubWindow() {
      delwin(m_Window);
    }
    
    virtual void refresh() {
      wrefresh(m_Window);
    }

    virtual void draw() = 0;
    virtual void handleInput(int c) = 0;

  protected:
    WINDOW* m_Parent;
    WINDOW* m_Window;
};