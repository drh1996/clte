#include "textWindow.h"

#include <iostream>
#include <string>
#include <fstream>

TextWindow::TextWindow(WINDOW* parent, int x, int y, int width, int height) 
  : SubWindow(parent, x, y, width, height) {

  m_Lines.push_back(new chstring());
}

TextWindow::~TextWindow() {
  //FOR DEBUG
  std::cerr << "Finished lines: " << std::endl;
  for (chstring* str : m_Lines) {
    std::cerr << *str << std::endl; 
  }
  
  clear();
}

void TextWindow::clear() {
  for (chstring* str : m_Lines) {
    delete str;
  }
  m_Lines.clear();
}

void TextWindow::loadFile(const char* filepath) {
  std::ifstream file(filepath);
  if (!file) {
    std::cout << "unable to open file";
    return;
  }

  clear(); //clear all lines

  std::string line;
  while (getline(file, line)) {
    chstring* str = new chstring(line);
    m_Lines.push_back(str);
  }
  m_CursorY = m_Lines.size() - 1;
  m_CursorX = m_Lines[m_CursorY]-> length();
}

void TextWindow::handleInput(int c) {
  std::cerr << "Keypress: '" << keyname(c) << "' (" << c << ')' << std::endl;
  switch (c) 
  {
    // case KEY_F(4): //handle F4 press
    //   std::cerr << "F4 PRESSED" << std::endl;
    case KEY_UP:
    case KEY_DOWN:
    case KEY_LEFT:
    case KEY_RIGHT:
      arrowKeys(c);
      break;
    case KEY_BACKSPACE:
    case 127:
    case '\b':
      backspace();
      break;
    case '\n':
      newLine();
      break;
    case KEY_RESIZE: //TODO: handle resizing
      std::cerr << "RESIZE OCCURRED: '" << c << "' COLS: " << COLS << ", LINES:" << LINES << std::endl;
      break;
    default:
      addChar(c);
  }
}

void TextWindow::draw() {
  wclear(m_Window);
  box(m_Window, 0, 0);
  // wborder(m_Window, 0, 0, 0, 0, 0, 0, 0, 0); 

  for (int i = 0; i < m_Lines.size(); i++) {
    mvwaddchnstr(m_Window, i + 1, 1, m_Lines[i]->data(), COLS - 2);
  }

  if (m_CursorX + 1 >= COLS) { //if text is off screen, set cursor to end of line
    wmove(m_Window, m_CursorY + 1, COLS - 1);
  } else {
    wmove(m_Window, m_CursorY + 1, m_CursorX + 1);
  }
}

void TextWindow::addChar(int c) {
  m_Lines[m_CursorY]->add(c, m_CursorX++);
}

void TextWindow::newLine() {
  chstring* currentSub = new chstring(*m_Lines[m_CursorY], 0, m_CursorX);
  chstring* toInsert = new chstring(*m_Lines[m_CursorY], m_CursorX);
  delete m_Lines[m_CursorY];
  m_Lines[m_CursorY] = currentSub;

  m_CursorY++;
  m_CursorX = 0;
  m_Lines.insert(m_Lines.begin() + m_CursorY, toInsert);
}

void TextWindow::backspace() {
  if (m_CursorX == 0 && m_CursorY > 0) {
    m_CursorX = m_Lines[m_CursorY - 1]->length();
    if (m_Lines[m_CursorY]->length() > 0) { //if your current line has some text in it, move it up to the previous line
      m_Lines[m_CursorY - 1]->append(*m_Lines[m_CursorY]);
    }
    m_Lines.erase(m_Lines.begin() + m_CursorY);
    m_CursorY--;
    
  } else if (m_CursorX > 0) { 
    chstring* str = m_Lines[m_CursorY];
    str->erase(m_CursorX - 1);
    m_CursorX--;
  }
}

void TextWindow::arrowKeys(int c) {
  if (c == KEY_UP) {
    if (m_CursorY > 0) {
      int lineLen = m_Lines[m_CursorY - 1]->length();
      m_CursorX = m_CursorX > lineLen ? lineLen : m_CursorX;
      m_CursorY--;
    } else { //move to start of line (0, 0)
      m_CursorX = 0;
    }
  } else if (c == KEY_DOWN) {
    if (m_CursorY < m_Lines.size() - 1) {
      int lineLen = m_Lines[m_CursorY + 1]->length();
      m_CursorX = m_CursorX > lineLen ? lineLen : m_CursorX;
      m_CursorY++;
    } else { //move to end of line
      m_CursorX = m_Lines[m_CursorY]->length();
    }
  } else if (c == KEY_LEFT) {
    if (m_CursorX > 0) {
      m_CursorX--;
    }
  } else if (c == KEY_RIGHT) {
    int endOfLine = m_Lines[m_CursorY]->length();
    if (m_CursorX < endOfLine) {
      m_CursorX++;
    }
  }
}