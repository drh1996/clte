#pragma once

#include <ncurses.h>
#include <vector>

#include "subwindow.h"
#include "chstring.h"

class TextWindow : public SubWindow {
  public:
    TextWindow(WINDOW* parent, int x, int y, int width, int height);
    ~TextWindow();

    void clear();
    void loadFile(const char* filepath);
    void handleInput(int c);
    void draw();

  private:
    void addChar(int c);
    void newLine();
    void backspace();
    void arrowKeys(int c);

  private:
    std::vector<chstring*> m_Lines;
    unsigned int m_CursorX;
    unsigned int m_CursorY;
};