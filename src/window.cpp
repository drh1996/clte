#include "window.h"

#include <iostream>
#include <fstream>

Window::Window()
{
  init();
}

Window::Window(const char* filepath)
{
  init();
  textWindow->loadFile(filepath);
  textWindow->draw();
  
  focusedWindow = commandWindow;
  refresh();
}

Window::~Window() 
{
  delete textWindow;
  delete commandWindow;
  delwin(mainWindow);
  endwin();
}

void Window::init() 
{
  mainWindow = initscr();
  if (!mainWindow) 
  {
    std::cerr << "Failed to initialize window." << std::endl;
  }

  textWindow = new TextWindow(mainWindow, 0, 0, COLS, LINES - 2);
  commandWindow = new CommandWindow(mainWindow, 0, LINES - 2, COLS, 2);
  noecho(); //dont print keystrokes
  raw(); //XON/XOFF off
  keypad(mainWindow, true);
  // box(textWindow, 0, 0); //TODO: look into subwindows (newwin(...) for border)
  // scrollok(window, true);
  // notimeout(window, true);
}

void Window::start() 
{
  int c;
  while (!commandWindow->shouldExitProgram())
  {
    c = getch();
    focusedWindow->handleInput(c);
    
    focusedWindow->draw();
    refresh();
  }
}

void Window::refresh() 
{
  wrefresh(mainWindow);
  focusedWindow->refresh();
}