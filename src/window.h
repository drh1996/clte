#pragma once

#include <string>
#include <vector>
#include <curses.h>

#include "commandWindow.h"
#include "textWindow.h"

class Window 
{
  public: 
    Window();
    Window(const char* filepath);
    ~Window();

    void start();
  private:
    void init();
    void refresh();
    
  private:
    WINDOW* mainWindow;

    SubWindow* focusedWindow;
    CommandWindow* commandWindow;
    TextWindow* textWindow;
};